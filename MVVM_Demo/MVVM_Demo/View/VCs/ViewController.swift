//
//  ViewController.swift
//  MVVM_Demo
//
//  Created by rahul on 19/04/21.
//

import UIKit

class ViewController: UIViewController {
    // development  second test
    // development new test
    //MARK:- IBOutlet
    @IBOutlet weak var photosView: UIView!
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var cvMain: UICollectionView!
    
    //MARK:- Custom variables
    
    var objImgs = [images]()
    var currentIndex : Int = 0
    var objUserVM: UserViewModel?
    var objuserData: UserData = UserData()
    var imagePicker = UIImagePickerController()
    var btnTapped: Int = -1
    
    //MARK:- View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableView.register(UINib.init(nibName: "ProfilePostSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfilePostSectionHeader")
        self.photosView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-225)
    }
    
    
    //MARK:- IBAction
    
    @IBAction func btnEditCoverImage(sender: UIButton) {
        btnTapped = 0
        ImagePickerManager().pickImage(self){ image in
            let cell = self.profileTableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! ProfileTableViewCell
            cell.imgCover.image = image
        }
    }
    @IBAction func btnEditProfilePic(sender: UIButton) {
        btnTapped = 1
        ImagePickerManager().pickImage(self){ image in
            let cell = self.profileTableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! ProfileTableViewCell
            cell.imgProfile.image = image
        }
    }
    @IBAction func btnSavesPost_clk(sender: UIButton) {
        cvMain.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .centeredHorizontally, animated: true)
    }
    @IBAction func btnCollections_clk(sender: UIButton) {
        cvMain.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 50))
        let vw = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfilePostSectionHeader") as! ProfilePostSectionHeader
        vw.btnCollection.tag = section
        vw.btnSavedPost.tag = section
        vw.btnCollection.addTarget(self, action: #selector(btnCollections_clk(sender:)), for: .touchUpInside)
        vw.btnSavedPost.addTarget(self, action: #selector(btnSavesPost_clk(sender:)), for: .touchUpInside)
        vw.lblSelectionCollection.isHidden = true
        view.addSubview(vw)
        view.backgroundColor = .red
        vw.frame = view.frame
        view = vw
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return UITableView.automaticDimension
        }else{
            return 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            cell.configuration(usermodel: UserData())
            cell.btnEditProfile.addTarget(self, action: #selector(btnEditProfilePic(sender:)), for: .touchUpInside)
            cell.btnEditProfile.tag = indexPath.row
            cell.btnEditCover.addTarget(self, action: #selector(btnEditCoverImage(sender:)), for: .touchUpInside)
            cell.btnEditCover.tag = indexPath.row
            return cell
        }else{
            return UITableViewCell.init(frame: .zero)
        }
    }
    
}

//MARK:- Collection view delegate and datasource methods

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_cvMain", for: indexPath) as! Cell_cvMain
        cell.index = indexPath
        cell.objPostVM = PostsViewModel.init(postImages: objuserData.savedPost)
        cell.objCollVM = CollectionViewModel.init(collectionImages: objuserData.collections)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width
        return CGSize.init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.currentIndex = indexPath.row

        let vw = profileTableView.headerView(forSection: 1) as! ProfilePostSectionHeader

        if indexPath.row == 0{
            vw.lblSelectionPost.isHidden = false
            vw.lblSelectionCollection.isHidden = true
        }else{
            vw.lblSelectionPost.isHidden = true
            vw.lblSelectionCollection.isHidden = false
           
        }
    }    
}
