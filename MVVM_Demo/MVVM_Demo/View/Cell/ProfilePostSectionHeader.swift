//
//  ProfilePostSectionHeader.swift
//  MVVM_Demo
//
//  Created by rahul on 20/04/21.
//

import UIKit

class ProfilePostSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var btnSavedPost: UIButton!
    @IBOutlet weak var lblSelectionPost: UILabel!
    @IBOutlet weak var btnCollection: UIButton!
    @IBOutlet weak var lblSelectionCollection: UILabel!

}

