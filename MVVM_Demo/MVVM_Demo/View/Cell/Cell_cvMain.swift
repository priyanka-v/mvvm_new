//
//  Cell_cvMain.swift
//  MVVM_Demo
//
//  Created by rahul on 19/04/21.
//

import UIKit

class Cell_cvPhotos: UICollectionViewCell {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var nslcImgTrailing: NSLayoutConstraint!
    @IBOutlet weak var nslcImgLeading: NSLayoutConstraint!
    
    //MARK:- Custom methods
    
    func config(objimg: images){
        imgPhoto.image = UIImage(named: objimg.image)
    }
}
class Cell_cvMain: UICollectionViewCell {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var cvPhotos: UICollectionView!
    
    //MARK:- Custom variables
    
    var index : IndexPath?
    var objPostVM: PostsViewModel?
    var objCollVM: CollectionViewModel?
    
}

//MARK:- Collection view delegate and datasource methods

extension Cell_cvMain : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index?.item == 0{
            return objPostVM?.postImages.count ?? 0
        }else{
            return objCollVM?.collectionImages.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_cvPhotos", for: indexPath) as! Cell_cvPhotos
        if index?.item == 0{
            print("red")
            if let postimgs = objPostVM?.postImages{
                cell.config(objimg: postimgs[indexPath.row])
            }
            cell.nslcImgLeading.constant = 0
            cell.nslcImgTrailing.constant = 0
        }else{
            if let collimgs = objCollVM?.collectionImages{
                cell.config(objimg: collimgs[indexPath.row])
            }
            cell.nslcImgLeading.constant = 10
            cell.nslcImgTrailing.constant = 10
            print("green")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width: CGFloat = 0.0
        var height: CGFloat = 0.0
        if index?.item == 0{
            width = collectionView.frame.size.width / 3 - 3
            height = width
        }else{
            width = collectionView.frame.size.width / 2
            height = 225
        }
        return CGSize.init(width: width, height: height)
    }
}
