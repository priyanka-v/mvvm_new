//
//  ProfileTableViewCell.swift
//  MVVM_Demo
//
//  Created by rahul on 20/04/21.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var btnMenubar: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblFollowings: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var btnEditCover: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    var btnTapped: Int = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configuration(usermodel: UserData){
        let objUserVM = UserViewModel.init(name: usermodel.name, id: usermodel.id, folllowers: usermodel.folllowers, bio: usermodel.bio, profileImage: usermodel.profileImage, coverImage: usermodel.coverImage)
        self.lblName.text = objUserVM.name
        self.lblId.text = objUserVM.id
        self.lblFollowings.text = objUserVM.folllowers
        self.lblBio.text = objUserVM.bio
        self.imgProfile.image = UIImage(named: objUserVM.profileImage)
        self.imgCover.image = UIImage(named: objUserVM.coverImage)
        
    }
}
