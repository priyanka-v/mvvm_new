//
//  Extensions.swift
//  MVVM_Demo
//
//  Created by rahul on 19/04/21.
//

import Foundation
import UIKit

@IBDesignable
extension UIView{
    @IBInspectable var cornerRadius: CGFloat{
        get{
            return self.layer.cornerRadius
        }
        set{
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = true
        }
    }
}
