//
//  UserData.swift
//  MVVM_Demo
//
//  Created by rahul on 19/04/21.
//

import Foundation

struct UserData {
    var name : String = "Vithani Priyanka"
    var id : String = "@vithanipriyanka"
    var folllowers : String = "35 Followings"
    var bio : String = "Hey I am using MeamerBox !"
    var profileImage : String = "profilePic"
    var coverImage : String = "appleBG"
    var savedPost : [images] = [images.init(image: "1"),images.init(image: "2"),images.init(image: "3"),images.init(image: "4"),images.init(image: "5")]
    var collections : [images] = [images.init(image: "1"),images.init(image: "2"),images.init(image: "3"),images.init(image: "4"),images.init(image: "5")]
}
struct images {
    var image : String
}
